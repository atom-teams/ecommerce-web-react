import { envConfig } from "./config";
// ### --------- ECOM API ----------- ###
// User
export const USER_SIGNIN = envConfig.ECOM_API + "users/signin";
export const USER_REGISTER = envConfig.ECOM_API + "users/register";

// product
export const PRODUCT_ADD = envConfig.ECOM_API + "products/add";
export const PRODUCT_LIST = envConfig.ECOM_API + "products";

// cart
export const CART_CRUD = envConfig.ECOM_API + "cart";
export const CART_LIST = envConfig.ECOM_API + "cart";
export const CART_DELETE = envConfig.ECOM_API + "cart/delete";