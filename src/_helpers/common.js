import SnackbarUtils from "_includes/SnackBar";
let cloudUrl = "https://d28cifwpfxyimh.cloudfront.net";

export const common = {
  // Url and Redirect
  url(url) {
    return process.env.PUBLIC_URL + url;
  },
  
  // basic
  snack(type, msg = "") {
    switch (type) {
      case "S":
        SnackbarUtils.success(msg);
        break;
      case "E":
        SnackbarUtils.error(msg);
        break;
      case "W":
        SnackbarUtils.warning(msg);
        break;
      case "I":
        SnackbarUtils.info(msg);
        break;
      case "T":
        SnackbarUtils.toast(msg);
        break;
      case "C":
        SnackbarUtils.closeAll();
        break;
    }
  },

  loadImg(img) {
    return require("../assets/img/" + img);
  },
};
