import moment from "moment";
const cryptoRandomString = require('crypto-random-string');
const dateFormat = "MMMM DD, YYYY hh:mm:ss A";
const utcFormat = "DD-MM-YYYY HH:mm:ss";

export const tools = {
  // date
  localNow() {
    return moment().format(dateFormat);
  },

  utcNow() {
    return moment().utc().format(utcFormat);
  },

  utcToLocal(date, format = dateFormat) {
    return moment.utc(date, utcFormat).local().format(format);
  },

  localToUtc(date) {
    return moment(date).utc().format(utcFormat);
  },

  // validate
  validateFields(val, accept){
    if(accept=="number"){
      val = this.onlyNumbers(val)
    } else if(accept=="float"){
      val = tools.onlyFloat(val)
    }
    return val;
  },

  ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    } else {
      return false;
    }
  },

  validateMobile(number) {
    return number.replace(/\D/g, "");
  },

  // input
  onlyNumbers(str) {
    return str.replace(/[^0-9]+/gi,"");
  },

  onlyFloat(str) {
    return str.replace(/[^0-9.]+/gi,"");
  },

  setEmpty(val){
    if(val=="" || val==null){
      return "";
    } else {
      return val;
    }
  },

  setDate(val,set=true){
    if(val=="" || val==null){
      if(set){
        return this.utcNow();
      } else {
        return ""
      }
    } else {
      return val;
    }
  },
  
  // dom
  scrollEnd(cls = "") {
    if (cls == "") {
      window.scrollTo(0, document.body.scrollHeight);
    } else {
      let ele = document.getElementsByClassName(cls);
      ele.scrollTo(0, ele.scrollHeight);
    }
  },
  
  // random string
  getIdentifier() {
    let ecomIdentifier = localStorage.getItem("ecomIdentifier");
    if (ecomIdentifier == null) {
      ecomIdentifier = cryptoRandomString({length: 20, type: 'alphanumeric'});
      localStorage.setItem("ecomIdentifier", ecomIdentifier)
    }
    return ecomIdentifier;
  },
};
