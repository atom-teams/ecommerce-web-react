import React from "react";
// Includes
import { StatusBar } from "_includes";
import { common, history, tools } from "_helpers";
import { apiServices } from "_api";

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      status: "process",
      isLoading: false,
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    var data = {
      url: "CART_LIST",
      query: "?referenceId=" + tools.getIdentifier(),
      method: "GET",
    };

    apiServices.call(
      data,
      (response) => {
        if (response.status === 200) {
          const { result } = response.data;
          let status = this.itemStatus(result);
          // set data
          this.setState({
            status,
            data: result.items,
          });
        } else {
          this.apiError();
        }
      },
      (error) => {
        console.log(error)
        this.apiError();
      },
      (final) => { }
    );
  };

  // CRUD Operations
  cartCrud = (key) => {
    this.setState({ isLoading: true });
    const { data } = this.state;
    const { productId, quantity } = data.items[key];
    if (this.checkQuantity(quantity)) {
      let fields = {
        "productId": productId,
        "referenceId": tools.getIdentifier(),
        "quantity": quantity
      };

      let data = {
        url: "CART_CRUD",
        body: fields,
      };

      apiServices.call(
        data,
        (response) => {
          if (this.crudStatus(response.status)) {
            this.crudSuccess();
          } else {
            this.crudError();
          }
        },
        (error) => {
          this.crudError();
        },
        (final) => {
          this.setState({ isLoading: false });
        }
      );
    }
  };

  // CRUD Operations
  cartRemove = (key) => {
    let res = window.confirm("Are you sure you want to delete this item?");
    if (res) {
      this.setState({ isLoading: true });
      const { data } = this.state;
      const { productId } = data.items[key];

      let params = {
        url: "CART_DELETE",
        query: "?productId=" + productId._id + "&referenceId=" + tools.getIdentifier(),
        method: "DELETE"
      };

      apiServices.call(
        params,
        (response) => {
          if (this.crudStatus(response.status)) {
            this.crudSuccess();
          } else {
            this.crudError();
          }
        },
        (error) => {
          this.crudError();
        },
        (final) => {
          this.setState({ isLoading: false });
        }
      );
    }
  };

  crudStatus(status) {
    if (status === 200) {
      return true;
    } else {
      return false;
    }
  }

  crudSuccess() {
    common.snack("S", "Cart updated successfully");
    this.loadData();
  }

  crudError() {
    common.snack("E", "There was an error, plesae try again");
    this.setState({
      isLoading: false,
    });
  }

  // handler
  handleCartChange = (event, key) => {
    const { data } = this.state;
    const { value } = event.target;
    data.items[key].quantity = value;
    this.setState({
      data
    });
  };

  // support functions
  checkQuantity(qty) {
    let allow = true;
    if (qty == "") {
      common.snack("E", "Please enter quantity and update");
      allow = false;
    } else {
      qty = parseInt(qty);
      if (qty <= 0 || qty > 200) {
        common.snack("W", "Quantity might between 1-200");
        allow = false;
      }
    }
    return allow;
  }

  itemStatus(result) {
    return result.totalResult == 0 ? "empty" : "success";;
  }

  apiError() {
    this.setState({
      status: "error",
      isLoading: false,
      data: [],
    });
  }

  redirect(page) {
    history.push(common.url(page));
  }

  render() {
    const {
      data,
      status,
      isLoading
    } = this.state;
    return (
      <section className="module">
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3">
              <h1 className="module-title font-alt">Checkout</h1>
            </div>
          </div>
          <label className="divider-w pt-20"></label>
          {status == "process" || status == "error" ? (
            <StatusBar status={status} />
          ) : status == "empty" ? (
            <div>
              <h4 className="font-alt mb-0">Cart Details</h4>
              <label className="divider-w mt-10 mb-20"></label>
              <div className="alert alert-info" role="alert"><strong>Cart!</strong> No items added to cart.</div>
              <div className="row">
                <div className="col-sm-3 col-sm-offset-0">
                  <div className="form-group">
                    <button className="btn btn-block btn-round btn-d pull-right" onClick={() => this.redirect("/")}>Add New Item</button>
                  </div>
                </div>
              </div>
            </div>
          ) : (
                <div>
                  <div className="row">
                    <div className="col-sm-12">
                      <table className="table table-striped table-border checkout-table">
                        <tbody>
                          <tr>
                            <th className="hidden-xs">Item</th>
                            <th>Description</th>
                            <th className="hidden-xs">Price</th>
                            <th>Quantity</th>
                            <th>Discount</th>
                            <th>Total</th>
                            <th>Update</th>
                            <th>Remove</th>
                          </tr>
                          {data.items.map((item, key) => (
                            <tr key={key}>
                              <td className="hidden-xs"><a href="#"><img src={common.loadImg(`shop/product${key > 3 ? "1" : key}.jpg`)} alt={item.productId.name} /></a></td>
                              <td>
                                <h5 className="product-title font-alt">{item.productId.name}</h5>
                              </td>
                              <td className="hidden-xs">
                                <h5 className="product-title font-alt">₹{item.price}</h5>
                              </td>
                              <td>
                                <input className="form-control" type="number" value={item.quantity} max={100} min={1} onChange={(e) => this.handleCartChange(e, key)} />
                              </td>
                              <td>
                                <h5 className="product-title font-alt">₹{item.discount}</h5>
                              </td>
                              <td>
                                <h5 className="product-title font-alt">₹{item.grandTotal}</h5>
                              </td>
                              <td>
                                <button className="btn btn-g btn-round btn-xs" type="submit" onClick={() => this.cartCrud(key)}>Update</button>
                              </td>
                              <td>
                                <button className="btn btn-g btn-round btn-xs" type="submit" onClick={() => this.cartRemove(key)}>Remove</button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-3">
                      <div className="form-group">
                        <input className="form-control" type="text" id="" name="" placeholder="Coupon code" />
                      </div>
                    </div>
                    <div className="col-sm-3">
                      <div className="form-group">
                        <button className="btn btn-round btn-g" type="submit">Apply</button>
                      </div>
                    </div>
                    <div className="col-sm-3 col-sm-offset-3">
                      <div className="form-group">
                        <button className="btn btn-block btn-round btn-d pull-right" onClick={() => this.redirect("/")}>Add New Item</button>
                      </div>
                    </div>
                  </div>
                  <label className="divider-w"></label>
                  <div className="row mt-70">
                    <div className="col-sm-5 col-sm-offset-7">
                      <div className="shop-Cart-totalbox">
                        <h4 className="font-alt">Cart Totals</h4>
                        <table className="table table-striped table-border checkout-table">
                          <tbody>
                            <tr>
                              <th>Sub Total :</th>
                              <td>₹{data.subTotal}</td>
                            </tr>
                            <tr>
                              <th>Cart Discount :</th>
                              <td>₹{data.cartDiscount} (-)</td>
                            </tr>
                            <tr>
                              <th>Shipping Total :</th>
                              <td>₹0</td>
                            </tr>
                            <tr className="shop-Cart-totalprice">
                              <th>Total :</th>
                              <td>₹{data.grandTotal}</td>
                            </tr>
                          </tbody>
                        </table>
                        <button className="btn btn-lg btn-block btn-round btn-d" type="submit">Proceed to Checkout</button>
                      </div>
                    </div>
                  </div>
                </div>
              )}
        </div>
      </section>
    );
  }
}
export { Cart };
