import React from "react";
// Includes
import { StatusBar } from "_includes";
import { common, history, tools } from "_helpers";
import { apiServices } from "_api";

// Import Images

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      status: "process",
      isLoading: false,
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    var data = {
      url: "PRODUCT_LIST",
      method: "GET",
    };

    apiServices.call(
      data,
      (response) => {
        if (response.status === 200) {
          const { result } = response.data;
          let status = this.itemStatus(result);
          // set data
          this.setState({
            status,
            data: result.items,
          });
        } else {
          this.apiError();
        }
      },
      (error) => {
        console.log(error)
        this.apiError();
      },
      (final) => { }
    );
  };

  // CRUD Operations
  cartCrud = (productId) => {
    this.setState({ isLoading: true });
    let fields = {
      "productId": productId,
      "referenceId": tools.getIdentifier(),
      "quantity": 1
    };

    let data = {
      url: "CART_CRUD",
      body: fields,
    };

    apiServices.call(
      data,
      (response) => {
        if (this.crudStatus(response.status)) {
          this.crudSuccess();
        } else {
          this.crudError();
        }
      },
      (error) => {
        this.crudError();
      },
      (final) => {
        this.setState({ isLoading: false });
      }
    );
  };

  crudStatus(status) {
    if (status === 200) {
      return true;
    } else {
      return false;
    }
  }

  crudSuccess() {
    common.snack("S", "Item has been added to cart");
    setTimeout(() => {
      history.push(common.url("cart"));
    }, 500);
  }

  crudError() {
    common.snack("E", "There was an error while adding item plesae try again");
    this.setState({
      isLoading: false,
    });
  }

  // support functions
  itemStatus(result) {
    return result.items.length == 0 ? "empty" : "success";;
  }

  apiError() {
    this.setState({
      status: "error",
      isLoading: false,
      data: [],
    });
  }

  render() {
    const {
      data,
      status,
      isLoading
    } = this.state;
    return (
      <div>
        <section className="module bg-dark-60 shop-page-header home-banner">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 col-sm-offset-3">
                <h2 className="module-title font-alt">Shop Your Products</h2>
                <div className="module-subtitle font-serif">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
              </div>
            </div>
          </div>
        </section>
        <label className="divider-w"></label>
        <StatusBar status={status} />
        {status == "success" && (
          <section className="module-small">
            <div className="container">
              <div className="row multi-columns-row">
                {data.map((item, key) => (
                  <div className="col-sm-6 col-md-3 col-lg-3" key={key}>
                    <div className="shop-item">
                      <div className="shop-item-image">
                        <img src={common.loadImg(`shop/product${key > 3 ? "1" : key}.jpg`)} alt={item.name} />
                        <div className="shop-item-detail">
                          <button className="btn btn-round btn-b" onClick={() => this.cartCrud(item._id)} disabled={isLoading}><span className="icon-basket">Add To Cart</span></button>
                        </div>
                      </div>
                      <h4 className="shop-item-title font-alt"><a href="#">{item.name}</a></h4>₹{item.price}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </section>
        )}
      </div>
    );
  }
}
export { Home };
