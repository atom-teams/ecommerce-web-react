import React from "react";
import { Router, Switch } from "react-router-dom";
import { common, history } from "_helpers";
import { EcomLayoutRoute } from "./routeLayout";

// pages
import {
  Home,
  Cart,
} from "../pages";

class RouterComponent extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <EcomLayoutRoute exact path={common.url("/")} component={Home} />
          <EcomLayoutRoute exact path={common.url("/cart")} component={Cart} />
        </Switch>
      </Router>
    );
  }
}

export  { RouterComponent };
