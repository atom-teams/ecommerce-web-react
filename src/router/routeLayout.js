import React from 'react';
import { Route } from 'react-router-dom';
//Import Layouts
import { EcomLayout } from '../_components/layouts';

//Admin Layout
export const EcomLayoutRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={matchProps => (
            <EcomLayout>
                <Component {...matchProps} />
            </EcomLayout>
        )} />
    )
};