import React from "react";
import { common } from "_helpers";

class StatusBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: this.props.status,
    };
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    return Object.keys(nextState).length
      ? (nextState.status = nextProps.status)
      : null;
  }

  render() {
    const { status } = this.state;
    return status == "process" ? (
      <div className="poStatusBar">
        <img
          src={common.loadImg("loader.gif")}
          alt={"Loader"}
          className="img100p img-fluid"
        />
        <p>Loading details please wait...</p>
      </div>
    ) : status == "error" ? (
      <div className="poStatusBar">
        <img
          src={common.loadImg("wrong.gif")}
          alt={"Loader"}
          className="img-fluid"
        />
      </div>
    ) : (
      status == "empty" && (
        <div className="poStatusBar">
          <img
            src={common.loadImg("noRecord.png")}
            alt={"Loader"}
            className="img-fluid"
          />
          <p>Sorry no record found</p>
        </div>
      )
    );
  }
}

export { StatusBar };
