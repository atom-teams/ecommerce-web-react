import React from 'react';
import moment from "moment";
class Footer extends React.Component {
    render() {
        return (
            <div>
                <label className="divider-d"></label>
                <footer className="footer bg-dark">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <p className="copyright font-alt">&copy; {moment().subtract(1,"year").format("YYYY")}-{moment().format("YYYY")}&nbsp;<a href="/">Ecommerce</a>, All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}
export { Footer };