import React from "react";
import { common, history } from "_helpers";

class Menu extends React.Component {

  render() {
    return (
      <nav className="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div className="container">
          <div className="navbar-header">
            <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span
              className="sr-only">Toggle navigation</span><span className="icon-bar"></span><span className="icon-bar"></span><span
                className="icon-bar"></span></button><a className="navbar-brand" href="/">Ecommerce</a>
          </div>
          <div className="collapse navbar-collapse" id="custom-collapse">
            <ul className="nav navbar-nav navbar-right">
              <li className="dropdown"><a className="dropdown-toggle" href="/" data-toggle="dropdown">Home</a></li>
              <li className="dropdown"><a className="dropdown-toggle" href="/" data-toggle="dropdown">Shop</a></li>
              <li className="dropdown"><a className="dropdown-toggle" href="/cart" data-toggle="dropdown">Cart</a></li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
export { Menu };