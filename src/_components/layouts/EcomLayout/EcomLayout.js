import React from 'react';
// Import Component
import { Menu, Header, Footer } from '_components';

class EcomLayout extends React.Component {
    render() {
        return (
            <div>
                <Menu />
                <div className="main">
                    <Header />
                    {this.props.children}
                    <Footer />
                </div>
            </div>
        )
    }
}
export { EcomLayout };